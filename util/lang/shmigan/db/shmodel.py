#!/usr/bin/env python3

# pylint: disable=W0613
# pylint: disable=C0413

import re
import enum

"""
Shmigan Dictionary Model

This module contains classes to work with shmigan data. On their own they often
have limited usefulness, but when tied together in their derived forms in the
shdata module they provide an easy method to work with a Shmigan database.

This module contains the ShmiganData class which instantiates a connection to
a database of shmigan words.

This class provides methods to easily manipulate the data in a shmigan database.

Additional classes are provided to interact at different granularities.
"""

class ShmiganFix(Enum):
    prefix = -1
    infix = 0
    suffix = 1

class ShmiganWord:
    """Contains relevant data for a Shmigan word. """

    _sort_repls = [
        ('xl', 'b'), ('lx', 'b'), ('shl', 'Z'), ('lsh', 'Z'), ('thl', 'Y'),
        ('lth', 'Y'), ('xr', 'X'), ('rx', 'X'), ('shr', 'W'), ('rsh', 'W'),
        ('thr', 'V'), ('rth', 'V'), ('spr', 'U'), ('rps', 'U'), ('skr', 'T'),
        ('rks', 'T'), ('str', 'S'), ('rts', 'S'), ('sn', 'R'), ('ns', 'R'),
        ('sl', 'Q'), ('ls', 'Q'), ('sr', 'P'), ('rs', 'P'), ('sx', 'O'),
        ('xs', 'O'), ('ssh', 'N'), ('shs', 'N'), ('sth', 'M'), ('ths', 'M'),
        ('sp', 'L'), ('ps', 'L'), ('sk', 'K'), ('ks', 'K'), ('st', 'J'),
        ('ts', 'J'), ('n', 'I'), ('l', 'H'), ('r', 'G'), ('x', 'F'),
        ('sh', 'E'), ('th', 'D'), ('p', 'C'), ('k', 'B'), ('t', 'A'),
        ('f', 'c'), ('j', 'd'), ('s', 'f'), ('m', 'g'), ('w', 'j'), ('h', 'k'),
        ('i', 'l'), ('e', 'm'), ('a', 'n'), ('u', 'p'), (' ', 'q')
    ]
    _sort_dict = dict(_sort_repls)
    _sort_regex = re.compile('|'.join([sh for (sh, _) in _sort_repls))
    _valid = [ # Valid shmigan syllable clusters, in replaceable order
        'xl', 'lx', 'shl', 'lsh', 'thl', 'lth', 'xr', 'rx', 'shr', 'rsh',
        'thr', 'rth', 'spr', 'rps', 'skr', 'rks', 'str', 'rts', 'sn', 'ns',
        'sl', 'ls', 'sr', 'rs', 'sx', 'xs', 'ssh', 'shs', 'sth', 'ths',
        'sp', 'ps', 'sk', 'ks', 'st', 'ts', 'n', 'l', 'r', 'x', 'sh', 'th',
        'p', 'k', 't', 'f', 'j', 's', 'm', 'w', 'h', 'i', 'e', 'a', 'u', 'o'
    ]
    _valid_regex = re.compile('(' + '|'.join(_valid) + '| )*')
    _local = [ # Local shmigan syllable clusters, in replaceable order
        'xl', 'lx', 'shl', 'lsh', 'thl', 'lth', 'xr', 'rx', 'shr', 'rsh',
        'thr', 'rth', 'spr', 'rps', 'skr', 'rks', 'str', 'rts', 'sn', 'ns',
        'sl', 'ls', 'sr', 'rs', 'sx', 'xs', 'ssh', 'shs', 'sth', 'ths',
        'sp', 'ps', 'sk', 'ks', 'st', 'ts', 'n', 'l', 'r', 'x', 'sh', 'th',
        'p', 'k', 't', 'i', 'e', 'a', 'u', 'o'
    ]
    _local_regex = re.compile('(' + '|'.join(_local) + '| )*')

    def __init__(
        self, spelling, pos, definition, description = None, root = None):
        self.spelling = spelling
        self.pos = pos
        self.definition = definition
        self.description = description
        self.root = root

    def shorder(self):
        """Returns a string representing this word in a sortable way."""
        return _sort_regex.sub(
            lambda match: _sort_dict[re.escape(match.group(0))], self._spelling)

    def is_valid(self):
        """Returns true if all letters in this word are valid."""
        return _valid_regex.match(self._spelling)

    def is_local(self):
        """Returns true if all letters in this word are local."""
        return _local_regex.match(self._spelling)

class ShmiganPOS:
    """Contains relevant data for a Shmigan part of speech."""
    def __init__(self, name):
        self.name = name

class ShmiganDM:
    """Contains relevant data for a Shmigan derivational morphology."""
    def __init__(self, spelling, meaning, fix, description = None):
        self.spelling = spelling
        self.meaning = meaning
        self.fix = fix
        self.description = description

class ShmiganPenta:
    """Contains relevant data for a Shmigan pentascale."""
    def __init__(self, words, root, name = None, iex = None, uex = None):
        """words is a dictionary from vowels to ShmiganWords"""
        self.words = words
        self.root = root
        self.name = name
        self.iex = iex
        self.uex = uex

class ShmiganDMPenta:
    """Contains relevant data for a Shmigan DM pentascale."""
    def __init__(self, morphs, root, name = None, iex = None, uex = None):
        """morphs is a dictionary from vowels to ShmiganDMs"""
        self.morphs = morphs
        self.root = root
        self.name = name
        self.iex = iex
        self.uex = uex

class ShmiganDoublePenta:
    """Contains relevant data for a Shmigan double pentascale."""
    def __init__(
        self, firsts, seconds, root, name = None, ixx = None, uxx = None,
        xix = None, xux = None):
        """firsts/seconds are dictionaries from vowels to ShmiganPentas"""
        self.firsts = firsts
        self.seconds = seconds
        self.root = root
        self.name = name
        self.ixx = ixx
        self.uxx = uxx
        self.xix = xix
        self.xux = xux

