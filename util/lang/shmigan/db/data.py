from enum import Enum
import sqlite3

class Fix(Enum):
    prefix = -1
    infix = 0
    suffix = 1

class Vowel(Enum):
    i = 0
    e = 1
    a = 2
    o = 3
    u = 4

class DBObj:
    _made = False
    _prefix = {}
    _table = None
    _refs = {}
    _idname = 'id'

    def __setattr__(self, attr, val):
        object.__setattr__(self, attr, val)

        if not self._made:
            return

        # Setting to a dict means setting a vowel variable use _prefix lookup
        if isinstance(val, dict):
            if attr in self._prefix:
                prefix = self._prefix[attr]

                for v in Vowel:
                    self._dictionary._db.execute(
                        f'UPDATE {self._table} '
                        f'SET {prefix + v.name()} = ? '
                        f'WHERE {self._idname} = ?', [val[v]._idx, self._idx])
            return

        if attr == 'fix':
            val = val.name()

        self._dictionary._db.execute(
            f'UPDATE {self._table} SET {attr} = ? WHERE {self._idname} = ?',
            [val._idx if attr in self._refs else val, self._idx])

    def delete(self):
        self._dictionary._db.execute(
            f'DELETE FROM {self._table} WHERE {self._idname} = ?', [self._idx])

class POS(DBObj):
    _table = 'Parts_Of_Speech'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.name = row[1]
        self._made = True

class Word(DBObj):
    _table = 'Dictionary'
    _refs = {'pos', 'root'}

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.spelling = row[1]
        self.definition = row[2]
        self.description = row[3]
        self.pos = parent.poses[row[4]]
        self._root_id = row[5]
        self._made = True

    def _load_root(self):
        self.root = self.dictionary.words[self._root_id]

class DM(DBObj):
    _table = 'Derivational_Morphologies'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.spelling = row[1]
        self.meaning = row[2]
        self.description = row[3]
        self.fix = Fix(row[4])
        self._made = True

class Derivation(DBObj):
    _table = 'Derivations'
    _idname = 'rowid'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.full = parent.words[row[1]]
        self.part = parent.dms[row[2]]
        self._made = True

class PentaDM(DBObj):
    _prefix = {'dm': ''}
    _table = 'Derivational_Pentascales'
    _idname = 'rowid'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.root = row[1]
        self.name = row[2]
        self.i_extreme = row[3]
        self.u_extreme = row[4]
        self.dm = {v.name: parent.dms[row[5 + v.value]] for v in Vowel}
        self._made = True

class Pentascale(DBObj):
    _prefix = {'word': ''}
    _table = 'Pentascales'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.root = row[1]
        self.name = row[2]
        self.i_extreme = row[3]
        self.u_extreme = row[4]
        self.word = {v.name: parent.words[row[5 + v.value]] for v in Vowel}
        self._made = True

class Doublescale(DBObj):
    _prefix = {'vx': 'first_', 'xv': 'second_'}
    _table = 'Double_Pentascales'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self.root = row[1]
        self.name = row[2]
        self.first_i_extreme = row[3]
        self.first_u_extreme = row[4]
        self.second_i_extreme = row[5]
        self.second_u_extreme = row[6]
        self.vx = {v.name: parent.pentas[row[7 + v.value]] for v in Vowel}
        self.xv = {v.name: parent.pentas[row[12 + v.value]] for v in Vowel}
        self._made = True

class Triplescale(DBObj):
    _prefix = {'vxx': 'first_', 'xvx': 'second_', 'xxv': 'third_'}
    _table = 'Triple_Pentascales'

    def __init__(self, parent, row):
        self._dictionary = parent
        self._idx = row[0]
        self._root = row[1]
        self.name = row[2]
        self.first_i_extreme = row[3]
        self.first_u_extreme = row[4]
        self.second_i_extreme = row[5]
        self.second_u_extreme = row[6]
        self.third_i_extreme = row[7]
        self.third_u_extreme = row[8]
        self.vxx = {v.name: parent.doubles[row[9 + v.value]] for v in Vowel}
        self.xvx = {v.name: parent.doubles[row[14 + v.value]] for v in Vowel}
        self.xxv = {v.name: parent.doubles[row[19 + v.value]] for v in Vowel}
        self._made = True

class Dictionary:
    def __init__(self, filename):
        self.words = {}
        self.poses = {}
        self.dms = {}
        self.derivations = []
        self.penta_dms = []
        self.pentas = {}
        self.doubles = {}
        self.triples = []

        self._db = sqlite3.connect(filename)

        for row in self._db.execute('SELECT * FROM Parts_Of_Speech'):
            self.poses[row[0]] = POS(self, row)

        for row in self._db.execute('SELECT * FROM Dictionary'):
            self.words[row[0]] = Word(self, row)

        for word in self.words.values():
            word._load_root()

        for row in self._db.execute('SELECT * FROM Derivational_Morphologies'):
            self.dms[row[0]] = DM(self, row)

        for row in self._db.execute('SELECT rowid, * FROM Derivations'):
            self.derivations.append(Derivation(self, row))

        for row in self._db.execute('SELECT rowid, * FROM Derivational_Pentascales'):
            self.penta_dms.append(PentaDM(self, row))

        for row in self._db.execute('SELECT * FROM Pentascales'):
            self.pentas[row[0]] = Pentascale(self, row)

        for row in self._db.execute('SELECT * FROM Double_Pentascales'):
            self.doubles[row[0]] = Doublescale(self, row)

        for row in self._db.execute('SELECT * FROM Triple_Pentascales'):
            self.triples.append(Triplescale(self, row))

    def save(self):
        self._db.commit()

    def close(self):
        self._db.close()

    def undo(self):
        self._db.rollback()

    # TODO: Add add_*()
    # TODO: Once all features are available, swap out model for this in view

