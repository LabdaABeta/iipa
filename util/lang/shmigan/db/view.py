#!/usr/bin/env python3

# pylint: disable=W0613
# pylint: disable=C0413

"""Shmigan Dictionary Viewer and Editor

This module is intended to be run from the command-line to invoke a GUI to view
and edit the Shmigan dictionary.
"""

from functools import reduce
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from model import *

class ViewList:
    _xml = 'shmigan.glade'

    def __init__(self, *objlist):
        self._builder = Gtk.Builder()
        self._builder.add_objects_from_file(self._xml, objlist)

    def connect(self, signals):
        self._builder.connect_signals(signals)

    def __getattr__(self, attr):
        return self._builder.get_object(attr)

    def get(self, attr):
        return self._builder.get_object(attr)

class NotebookTab:
    def __init__(self, tab, index, label, parent, item = None):
        self._tab = tab
        self._index = index
        self._label = label
        self._parent = parent
        self.item = item

        self._initialize_fields()

    def __getattr__(self, attr):
        return self._tab.__getattr__(attr)

    def get(self, attr):
        return self._tab.get(attr)

    def _initialize_fields(self):
        pass

    def _detach(self):
        self._parent.nbMain.remove_page(self._index)

    def _confirm(self, title, text):
        return self._parent.confirm_dialog(title, text)

    def _choice_dialog(self, title, text, choices):
        return self._parent.choice_dialog(title, text, choices)

def update_search(view):
    """ Updates the view's search bar using its search and searchtype fields.
    """
    search = view.entSearch.get_text()
    search_type = view.cbSearchType.get_active_id()

    if search_type == '1':
        word_fields = ['definition']
        dm_fields = ['meaning']
    elif search_type == '2':
        word_fields = ['spelling']
        dm_fields = ['spelling']
    else:
        word_fields = ['spelling', 'definition']
        dm_fields = ['spelling', 'meaning']

    model = view.tsSearchResults
    model.clear()

    for word in ShmiganWord.text_search(word_fields, search):
        model.append(None, [word.spelling, word.definition, word.id,
            ShmiganWord.ordered(word.spelling), 'Word', word.pos.name])

    for dm in ShmiganDM.text_search(dm_fields, search):
        model.append(None, [dm.spelling, dm.meaning, dm.id,
            ShmiganWord.ordered(dm.spelling), 'DM', ''])

class CreatePOSHandler(NotebookTab):
    """Contains handlers and initialization for the Create POS tab."""
    def _initialize_fields(self):
        list_model = self.tvPOSList.get_model()

        for pos in ShmiganPOS.all():
            list_model.append(None, [pos.name])

    def cancel_add_pos(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_add_pos(self, *args):
        """Handler for clicking on the Apply button"""
        ShmiganPOS.insert(name = self.entPOS.get_text())
        self._detach()

class WordHandler(NotebookTab):
    """Contains handlers and initialization for the Create or Edit Word tab."""
    def _initialize_fields(self):
        for pos in ShmiganPOS.all():
            self.cbPOS.append(str(pos.id), pos.name)

        if self.item is not None:
            self.entSpelling.set_text(self.item.spelling)
            self.update_spelling()
            self.entDefinition.set_text(self.item.definition)
            if self.item.description:
                self.tvDescription.get_buffer().set_text(self.item.description)
            self.cbPOS.set_active_id(str(self.item.pos.id))

            model = self.tsScales
            model.clear()

            for penta in ShmiganPenta.find('ieaou', self.item.id):
                model.append(None, [penta.id, penta.root, penta.name,
                    penta.i_extreme, penta.u_extreme])
        else:
            self.btnDeleteWord.set_sensitive(False)

    def cancel_word(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_word(self, *args):
        """Handler for clicking on the Apply button"""
        posid = self.cbPOS.get_active_id()
        pos = ShmiganPOS(posid)
        spelling = self.entSpelling.get_text()
        buf = self.tvDescription.get_buffer()
        desc = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), True)
        definition = self.entDefinition.get_text()

        # Verify data first
        if not posid:
            return self._confirm(
                'NO POS!',
                'You did not set the part of speech. Cannot continue.')
        if not ShmiganWord.is_valid(spelling):
            if not self._confirm(
                'Are you sure?',
                'The spelling is not valid. Continue?'):
                return
        if not ShmiganWord.is_local(spelling):
            if not self._confirm(
                'Are you sure?',
                'The spelling is not local. Continue?'):
                return
        if not definition:
            if not self._confirm(
                'Are you sure?',
                'The definition is missing. Continue?'):
                return

        if self.item is not None:
            self.item.spelling = spelling
            self.item.definition = definition
            self.item.description = desc
            self.item.pos = pos
        else:
            ShmiganWord.insert(
                spelling = spelling,
                definition = definition,
                description = desc,
                pos = pos)

        update_search(self._parent)
        self._detach()

    def delete_word(self, *args):
        """Handler for clicking on the Delete button"""
        self.item.delete()
        update_search(self._parent)
        self._detach()

    def update_spelling(self, *args):
        """Handler for editing the spelling field"""
        spelling = self.entSpelling.get_text()

        self.lblShmigan.set_text(spelling + ' ')
        self.lblUgashmigan.set_text(spelling)
        self.lblPenShmigan.set_text(spelling)
        self.lblKashmigan.set_text(spelling)

        if self.item is None:
            self._label.set_text(spelling + ' [+]')
        else:
            self._label.set_text(spelling)

    def activate_scale_row(self, treeview, path, column):
        """Handler for double-clicking in the pentascales list"""
        model = treeview.get_model()
        treeiter = model.get_iter(path)

        if treeiter is not None:
            sel = ShmiganPenta(model[treeiter][0])

            tab = ViewList('pgPentascale', 'tsScales')

            new_page = tab.pgPentascale
            page_label = Gtk.Label.new(model[treeiter][1])

            index = self._parent.add_page(new_page, page_label)
            tab.connect(PentaHandler(tab, index, page_label, self._parent, sel))

class DMHandler(NotebookTab):
    """Contains handlers and initialization for the Create or Edit DM tab."""
    def _initialize_fields(self):
        if self.item is not None:
            data = self._view.sql(
                'SELECT spelling, meaning, description, fix '
                'FROM Derivational_Morphologies '
                'WHERE id = ?',
                [self._idx]).fetchone()

            self.entDSpelling.set_text(self.item.spelling)
            self.entDDefinition.set_text(self.item.meaning)
            self.tvDDescription.get_buffer().set_text(self.item.description)
            self.cbFix.set_active_id(str(self.item.fix))
        else:
            self.btnDeleteDM.set_sensitive(False)

    def cancel_dm(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_dm(self, *args):
        """Handler for clicking on the Apply button"""
        spelling = self.entDSpelling.get_text()
        definition = self.entDDefinition.get_text()
        buf = self.tvDDescription.get_buffer()
        fix = self.cbFix.get_active_id()
        desc = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), True)

        if self.item is not None:
            self.item.spelling = spelling
            self.item.definition = definition
            self.item.description = desc
            self.item.fix = fix
        else:
            ShmiganDM.insert(
                spelling = spelling,
                meaning = definition,
                description = desc,
                fix = fix)

        update_search(self._parent)
        self._detach()

    def delete_dm(self, *args):
        """Handler for clicking on the Delete button"""
        self.item.delete()
        update_search(self._parent)
        self._detach()

    def update_spelling(self, *args):
        """Handler for editing the spelling field"""
        spelling = self.entDSpelling.get_text()
        fix = self._tab.get_object('cbFix').get_active_id()

        if fix != '-1':
            spelling = '-' + spelling

        if fix != '1':
            spelling = spelling + '-'

        self.lblDShmigan.set_text(spelling)
        self.lblDUgashmigan.set_text(spelling)
        self.lblDPenShmigan.set_text(spelling)
        self.lblDKashmigan.set_text(spelling)

        if self.item is None:
            self._label.set_text(spelling + ' [+]')
        else:
            self._label.set_text(spelling)

class PentaHandler(NotebookTab):
    """Contains handlers and initialization for the Create or Edit Pentascale
    tab."""
    def _initialize_fields(self):
        for pos in ShmiganPOS.all():
            for v in 'IEAOU':
                self.get(f'cb{v}POS').append(str(pos.id), pos.name)

        if self.item is not None:
            self.entRoot.set_text(self.item.root)
            self.update_spelling()
            self.entName.set_text(self.item.name)
            self.entIExtreme.set_text(self.item.i_extreme)
            self.entUExtreme.set_text(self.item.u_extreme)

            for v in 'ieaou':
                V = v.upper()
                w = self.item.__getattr__(v)
                self.get(f'lbl{V}Word').set_text(w.spelling)
                self.get(f'ent{V}Definition').set_text(w.definition)
                if w.description:
                    self.get(f'tv{V}Description').get_buffer().set_text(w.description)
                self.get(f'cb{V}POS').set_active_id(str(w.pos.id))

            model = self.tsScales
            model.clear()

            for dpenta in ShmiganDoublePenta.find([
                'first_i', 'first_e', 'first_a', 'first_o', 'first_u',
                'second_i', 'second_e', 'second_a', 'second_o', 'second_u'],
                self.item.id):
                model.append(None, [dpenta.id, dpenta.root, dpenta.name,
                    dpenta.first_i_extreme + '/' + dpenta.second_i_extreme,
                    dpenta.first_u_extreme + '/' + dpenta.second_u_extreme])
        else:
            self.btnDeletePentascale.set_sensitive(False)

    def cancel_pentascale(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_pentascale(self, *args):
        """Handler for clicking on the Apply button"""
        root = self.entRoot.get_text()
        name = self.entName.get_text()
        iex = self.entIExtreme.get_text()
        uex = self.entUExtreme.get_text()
        spell = {x: root.replace(' ', x, 1) for x in 'ieaou'}
        defn = {x: self.get(f'ent{x.upper()}Definition').get_text()
            for x in 'ieaou'}
        bufs = {x: self.get(f'tv{x.upper()}Description').get_buffer()
            for x in 'ieaou'}
        descs = {x: y.get_text(y.get_start_iter(), y.get_end_iter(), True)
            for (x,y) in bufs.items()}
        posid = {x: self.get(f'cb{x.upper()}POS').get_active_id()
            for x in 'ieaou'}
        pos = {x: ShmiganPOS(y) for (x,y) in posid.items()}

        # Verify data first
        if reduce(lambda a,b: a or b, [not x for _,x in posid.items()], False):
            return self._confirm(
                'NO POS!',
                'You did not set one or more parts of speech. Cannot continue.')
        if not ShmiganWord.is_valid(root.replace(' ', '', 1)):
            if not self._confirm(
                'Are you sure?',
                'The spelling is not valid. Continue?'):
                return
        if not ShmiganWord.is_local(root.replace(' ', '', 1)):
            if not self._confirm(
                'Are you sure?',
                'The spelling contains foreign letters. Continue?'):
                return
        if reduce(lambda a,b: a or b, [not x for _,x in posid.items()], False):
            if not self._confirm(
                'Are you sure?',
                'You are missing one or more definitions. '
                'Continue?'):
                return

        if self.item is not None:
            for v in 'ieaou':
                self.item.__getattr__(v).spelling = spell[v]
                self.item.__getattr__(v).definition = defn[v]
                self.item.__getattr__(v).description = descs[v]
                self.item.__getattr__(v).pos = pos[v]
            self.item.root = root
            self.item.name = name
            self.item.i_extreme = iex
            self.item.u_extreme = uex
        else:
            ShmiganPenta.insert(
                root = root, name = name, i_extreme = iex, u_extreme = uex, **{
                    x: ShmiganWord.insert(
                        spelling = spell[x],
                        definition = defn[x],
                        description = descs[x],
                        pos = pos[x]) for x in 'ieaou'})

        update_search(self._parent)
        self._detach()

    def delete_pentascale(self, *args):
        """Handler for clicking on the Delete button"""
        action = self._choice_dialog(
            'Delete what?',
            'Do you want to delete just the scale, or the words in it as well?',
            ['Just the scale', 'The words too', 'Cancel'])

        if action == 'Just the scale':
            self.item.delete()
        elif action == 'The words too':
            for x in 'ieaou':
                self.item.__getattr__(x).delete()
            self.item.delete()
        else:
            return

        update_search(self._parent)
        self._detach()

    def update_spelling(self, *args):
        """Handler for editing the spelling field"""
        spelling = self.entRoot.get_text()

        self.lblShmigan1.set_text(spelling + ' ')
        self.lblUgashmigan1.set_text(spelling)
        self.lblPenShmigan1.set_text(spelling)
        self.lblKashmigan1.set_text(spelling)
        self.lblIWord.set_text(spelling.replace(' ', 'i', 1))
        self.lblEWord.set_text(spelling.replace(' ', 'e', 1))
        self.lblAWord.set_text(spelling.replace(' ', 'a', 1))
        self.lblOWord.set_text(spelling.replace(' ', 'o', 1))
        self.lblUWord.set_text(spelling.replace(' ', 'u', 1))

        if self.item is None:
            self._label.set_text(spelling + ' [+]')
        else:
            self._label.set_text(spelling)

    def activate_scale_row(self, treeview, path, column):
        """Handler for double-clicking in the double pentascales list"""
        model = treeview.get_model()
        treeiter = model.get_iter(path)

        if treeiter is not None:
            sel = ShmiganDoublePenta(model[treeiter][0])

            tab = ViewList('pgDoublePentascale')

            new_page = tab.pgDoublePentascale
            page_label = Gtk.Label.new(model[treeiter][1])

            index = self._parent.add_page(new_page, page_label)
            tab.connect(DoubleHandler(tab, index, page_label, self._parent, sel))

class PentaDMHandler(NotebookTab):
    """Contains handlers and initialization for the Create or Edit Pentascale
    tab."""
    def _initialize_fields(self):
        if self.item is not None:
            self.entDPRoot.set_text(self.item.root)
            self.update_spelling()
            self.entName1.set_text(self.item.name)
            self.entIExtreme1.set_text(self.item.i_extreme)
            self.entUExtreme1.set_text(self.item.u_extreme)

            for v in 'ieaou':
                V = v.upper()
                w = self.item.__getattr__(v)
                self.get(f'lbl{V}Word1').set_text(w.spelling)
                self.get(f'ent{V}Definition1').set_text(w.meaning)
                if w.description:
                    self.get(f'tv{V}Description1').get_buffer().set_text(w.description)
        else:
            self.btnDeletePentaDM.set_sensitive(False)

    def cancel_pentadm(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_pentadm(self, *args):
        """Handler for clicking on the Apply button"""
        root = self.entDPRoot.get_text()
        name = self.entName1.get_text()
        iex = self.entIExtreme1.get_text()
        uex = self.entUExtreme1.get_text()
        spell = {x: root.replace(' ', x, 1) for x in 'ieaou'}
        meaning = {x: self.get(f'ent{x.upper()}Definition1').get_text()
                for x in 'ieaou'}
        bufs = {x: self.get(f'tv{x.upper()}Description1').get_buffer()
                for x in 'ieaou'}
        descs = {x: y.get_text(y.get_start_iter(), y.get_end_iter(), True)
                for (x,y) in bufs.items()}
        fix = self.cbPentaFix.get_active_id()

        # TODO: add checks from PentaHandler that are relevant

        if self.item is not None:
            for v in 'ieaou':
                self.item.__getattr__(v).spelling = spell[v]
                self.item.__getattr__(v).meaning = meaning[v]
                self.item.__getattr__(v).description = descs[v]
                self.item.__getattr__(v).fix = fix
            self.item.root = root
            self.item.name = name
            self.item.i_extreme = iex
            self.item.u_extreme = uex
        else:
            ShmiganDMPenta.insert(
                root = root, name = name, i_extreme = iex, u_extreme = uex, **{
                    x: ShmiganDM.insert(
                        spelling = spell[x],
                        meaning = meaning[x],
                        description = descs[x],
                        fix = fix) for x in 'ieaou'})

        update_search(self._parent)
        self._detach()

    def delete_pentadm(self, *args):
        """Handler for clicking on the Delete button"""
        action = self._choice_dialog(
            'Delete what?',
            'Do you want to delete just the scale, or the DMs in it as well?',
            ['Just the scale', 'The words too', 'Cancel'])

        if action == 'Just the scale':
            self.item.delete()
        elif action == 'The words too':
            for x in 'ieaou':
                self.item.__getattr__(x).delete()
            self.item.delete()
        else:
            return

        update_search(self._parent)
        self._detach()

    def update_spelling(self, *args):
        """Handler for editing the spelling field"""
        spelling = self.entDPRoot.get_text()

        self.lblShmigan3.set_text(spelling + ' ')
        self.lblUgashmigan3.set_text(spelling)
        self.lblPenShmigan3.set_text(spelling)
        self.lblKashmigan3.set_text(spelling)
        self.lblIWord1.set_text(spelling.replace(' ', 'i', 1))
        self.lblEWord1.set_text(spelling.replace(' ', 'e', 1))
        self.lblAWord1.set_text(spelling.replace(' ', 'a', 1))
        self.lblOWord1.set_text(spelling.replace(' ', 'o', 1))
        self.lblUWord1.set_text(spelling.replace(' ', 'u', 1))

        if self.item is None:
            self._label.set_text(spelling + ' [+]')
        else:
            self._label.set_text(spelling)



class DoubleHandler(NotebookTab):
    """Contains handlers and initialization for the Create or Edit Double
    Pentascale tab."""
    def _initialize_fields(self):
        for pos in ShmiganPOS.all():
            self.cbAllDoublePOS.append(str(pos.id), pos.name)

        if self.item is not None:
            self.entDRoot.set_text(self.item.root)
            self.update_spelling()
            self.entDName.set_text(self.item.name)
            self.entFirstIExtreme.set_text(self.item.first_i_extreme)
            self.entFirstUExtreme.set_text(self.item.first_u_extreme)
            self.entSecondIExtreme.set_text(self.item.second_i_extreme)
            self.entSecondUExtreme.set_text(self.item.second_u_extreme)

            self.cbAllDoublePOS.set_active_id(str(self.item.first_i.i.pos.id))

            for v in 'ieaou':
                V = v.upper()
                first = self.item.__getattr__('first_' + v)
                second = self.item.__getattr__('second_' + v)

                self.get(f'entFirst{V}Name').set_text(first.name)
                self.get(f'entSecond{V}Name').set_text(second.name)

                for v2 in 'ieaou':
                    V2 = v2.upper()
                    w = first.__getattr__(v2)
                    self.get(f'ent{V}{V2}Definition').set_text(w.definition)
        else:
            self.btnDeleteDoublePentascale.set_sensitive(False)

    def cancel_double_pentascale(self, *args):
        """Handler for clicking on the Cancel button"""
        self._detach()

    def apply_double_pentascale(self, *args):
        """Handler for clicking on the Apply button"""
        root = self.entDRoot.get_text()
        name = self.entDName.get_text()
        ixex = self.entFirstIExtreme.get_text()
        uxex = self.entFirstUExtreme.get_text()
        xiex = self.entSecondIExtreme.get_text()
        xuex = self.entSecondUExtreme.get_text()
        pent_names = {}
        for v in 'ieaou':
            V = v.upper()
            pent_names[v + 'x'] = self.get(f'entFirst{V}Name').get_text()
            pent_names['x' + v] = self.get(f'entSecond{V}Name').get_text()
        spell = {v1+v2: root.replace(' ', v1, 1).replace(' ', v2, 1)
            for v1 in 'ieaou' for v2 in 'ieaou'}
        defs = {v1+v2: self.get(f'ent{v1}{v2}Definition').get_text()
            for v1 in 'IEAOU' for v2 in 'IEAOU'}
        posid = self.cbAllDoublePOS.get_active_id()
        pos = ShmiganPOS(posid)

        if not posid:
            return self._confirm(
                'NO POS!',
                'You did not set the part of speech. Cannot continue.')
        if not ShmiganWord.is_valid(root.replace(' ', '', 2)):
            if not self._confirm(
                'Are you sure?',
                'The spelling is not valid. Continue?'):
                return
        if not ShmiganWord.is_local(root.replace(' ', '', 2)):
            if not self._confirm(
                'Are you sure?',
                'The spelling contains foreign letters. Continue?'):
                return
        if reduce(lambda a,b: a or b, [not x for _,x in defs.items()], False):
            if not self._confirm(
                'Are you sure?',
                'You are missing one or more definitions. '
                'Continue?'):
                return

        if self.item is not None:
            self.item.root = root
            self.item.name = name
            self.item.first_i_extreme = ixex
            self.item.first_u_extreme = uxex
            self.item.second_i_extreme = xiex
            self.item.second_u_extreme = xuex
            for v in 'ieaou':
                first = self.item.__getattr__(f'first_{v}')
                second = self.item.__getattr__(f'second_{v}')

                first.root = root.replace(' ', v, 1)
                first.name = pent_names[v + 'x']
                first.i_extreme = ixex
                first.u_extreme = uxex

                second.root = v.join(root.rsplit(' ', 1))
                second.name = pent_names['x' + v]
                second.i_extreme = xiex
                second.u_extreme = xuex

                for v2 in 'ieaou':
                    w = first.__getattr__(v2)
                    w.spelling = spell[v + v2]
                    w.definition = defs[v + v2]
                    w.pos = pos
        else:
            word_ids = {v + v2: ShmiganWord.insert(spelling = spell[v + v2],
                definition = defs[v.upper() + v2.upper()], pos = pos)
                for v in 'ieaou' for v2 in 'ieaou'}
            ShmiganDoublePenta.insert(
                root = root, name = name,
                first_i_extreme = ixex, first_u_extreme = uxex,
                second_i_extreme = xiex, second_u_extreme = xuex, **{
                    'first_' + v: ShmiganPenta.insert(
                        root = root.replace(' ', v, 1),
                        name = pent_names[v + 'x'],
                        i_extreme = ixex, u_extreme = uxex, **{
                            x: word_ids[v + x] for x in 'ieaou'})
                    for v in 'ieaou'}, **{
                    'second_' + v: ShmiganPenta.insert(
                        root = v.join(root.rsplit(' ', 1)),
                        name = pent_names['x' + v],
                        i_extreme = xiex, u_extreme = xuex, **{
                            x: word_ids[x + v] for x in 'ieaou'})
                    for v in 'ieaou'})

        update_search(self._parent)
        self._detach()

    def delete_double_pentascale(self, *args):
        """Handler for clicking on the Delete button"""
        action = self._choice_dialog(
            'Delete what?',
            'Do you want to delete just the scale, or the words in it as well?',
            ['Just this scale', 'Just the scales', 'The words too', 'Cancel'])

        if action == 'Just this scale':
            self.item.delete()
        elif action == 'Just the scales':
            for x in 'ieaou':
                self.item.__getattr__(f'first_{x}').delete()
                self.item.__getattr__(f'second_{x}').delete()
            self.item.delete()
        elif action == 'The words too':
            for x in 'ieaou':
                first = self.item.__getattr__(f'first_{x}')
                for y in 'ieaou':
                    first.__getattr__(y).delete()
                first.delete()
                self.item.__getattr__(f'second_{x}').delete()
            self.item.delete()

        update_search(self._parent)
        self._detach()

    def update_spelling(self, *args):
        """Handler for editing the spelling field"""
        spelling = self.entDRoot.get_text()
        spell = {v1+v2: spelling.replace(' ', v1, 1).replace(' ', v2, 1)
            for v1 in 'ieaou' for v2 in 'ieaou'}

        for v, w in spell.items():
            self.get(f'lbl{v.upper()}Word').set_text(w)

        self.lblShmigan2.set_text(spelling + ' ')
        self.lblUgashmigan2.set_text(spelling)
        self.lblPenShmigan2.set_text(spelling)
        self.lblKashmigan2.set_text(spelling)

        if self.item is None:
            self._label.set_text(spelling + ' [+]')
        else:
            self._label.set_text(spelling)

class MainHandler:
    """Contains handlers and initialization for the main window."""
    def __init__(self, view):
        self._view = view
        update_search(self._view)

    def on_destroy(self, *args):
        """Handler for exiting the application"""
        DB.commit()
        DB.close()
        Gtk.main_quit()

    def save_data(self, *args):
        """Handler for clicking on the save button"""
        DB.commit()
        update_search(self._view)

    def undo_data(self, *args):
        """Handler for clicking on the undo button"""
        DB.rollback()
        update_search(self._view)

    def update_search(self, *args):
        """Handler for editing the search field"""
        update_search(self._view)

    def create_pos(self, *args):
        """Handler for the Create POS toolbar item"""
        tab = ViewList('pgCreatePOS', 'tsText')

        new_page = tab.pgCreatePOS
        page_label = Gtk.Label.new('Create POS')

        index = self._view.add_page(new_page, page_label)
        tab.connect(CreatePOSHandler(tab, index, page_label, self._view))

    def create_word(self, *args):
        """Handler for the Create Word toolbar item"""
        tab = ViewList('pgWord', 'tbDescription', 'tsScales')

        new_page = tab.pgWord
        page_label = Gtk.Label.new('Create Word')

        index = self._view.add_page(new_page, page_label)
        tab.connect(WordHandler(tab, index, page_label, self._view))

    def create_dm(self, *args):
        """Handler for the Create DM toolbar item"""
        tab = ViewList('pgDM', 'tbDescription')

        new_page = tab.pgDM
        page_label = Gtk.Label.new('Create DM')

        index = self._view.add_page(new_page, page_label)
        tab.connect(DMHandler(tab, index, page_label, self._view))

    def create_penta(self, *args):
        """Handler for the Create Pentascale toolbar item"""
        tab = ViewList('pgPentascale', 'tsScales')

        new_page = tab.pgPentascale
        page_label = Gtk.Label.new('Create Pentascale')

        index = self._view.add_page(new_page, page_label)
        tab.connect(PentaHandler(tab, index, page_label, self._view))

    def create_pentad(self, *args):
        """Handler for the Create PentaDM toolbar item"""
        tab = ViewList('pgPentaDM')

        new_page = tab.pgPentaDM
        page_label = Gtk.Label.new('Create PentaDM')

        index = self._view.add_page(new_page, page_label)
        tab.connect(PentaDMHandler(tab, index, page_label, self._view))

    def create_double_penta(self, *args):
        """Handler for the Create Double Pentascale toolbar item"""
        tab = ViewList('pgDoublePentascale')

        new_page = tab.pgDoublePentascale
        page_label = Gtk.Label.new('Create Double')

        index = self._view.add_page(new_page, page_label)
        tab.connect(DoubleHandler(tab, index, page_label, self._view))

    def activate_search_row(self, treeview, path, column):
        """Handler for double-clicking in the search results"""
        model = treeview.get_model()
        treeiter = model.get_iter(path)

        if treeiter is not None:
            kind = model[treeiter][4]

            if kind == 'Word':
                tab = ViewList('pgWord', 'tbDescription', 'tsScales')

                new_page = tab.pgWord
                page_label = Gtk.Label.new(model[treeiter][0])

                index = self._view.add_page(new_page, page_label)
                sel = ShmiganWord(model[treeiter][2])
                tab.connect(WordHandler(tab, index, page_label, self._view, sel))
            elif kind == 'DM':
                tab = self._view.build(['pgDM', 'tbDescription'])

                new_page = tab.get_object('pgDM')
                page_label = Gtk.Label.new(model[treeiter][0])

                index = self._view.add_page(new_page, page_label)
                sel = ShmiganDM(model[treeiter][2])
                tab.connect(DMHandler(tab, index, page_label, self._view, sel))
            else:
                print('Unknown type:', kind)

class ShmiganView(ViewList):
    """Contains relevant data for the whole view. This includes the database."""
    def __init__(self):
        ViewList.__init__(self, 'wndMain', 'tsSearchResults')
        self.connect(MainHandler(self))

    def run(self):
        """Runs the application"""
        self.wndMain.show_all()
        Gtk.main()

    def add_page(self, page, label):
        """Adds a page to the main notebook"""
        close_image = Gtk.Image.new_from_stock(
            Gtk.STOCK_CLOSE, Gtk.IconSize.BUTTON)

        kill_button = Gtk.Button.new()
        kill_button.set_image(close_image)
        kill_button.set_relief(Gtk.ReliefStyle.NONE)
        kill_button.show()

        label.show()

        real_label = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 3)
        real_label.pack_start(label, True, True, 0)
        real_label.pack_start(kill_button, False, False, 0)
        real_label.show()

        index = self.nbMain.append_page(page, real_label)

        kill_button.connect(
            'clicked',
            lambda *args: self.nbMain.remove_page(index))

        self.nbMain.set_current_page(index)
        return index

    def confirm_dialog(self, title, text):
        """Make a dialog popup for Ok or Cancel. Returns True on 'OK'"""
        dialog = Gtk.Dialog(
            title, self._builder.get_object('wndMain'), 0,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK))

        label = Gtk.Label.new(text)
        dialog.get_content_area().add(label)
        dialog.show_all()

        response = dialog.run()

        dialog.destroy()

        return response == Gtk.ResponseType.OK

    def choice_dialog(self, title, text, choices):
        """Make a dialog popup for the text options in choices, returning the
        selected choice as a string"""
        dialog = Gtk.Dialog(title, self._builder.get_object('wndMain'), 0)

        label = Gtk.Label.new(text)
        dialog.get_content_area().add(label)

        for i in range(len(choices)):
            dialog.add_button(choices[i], i)

        dialog.show_all()

        response = dialog.run()

        dialog.destroy()

        return choices[response]

if __name__ == '__main__':
    the_view = ShmiganView()
    the_view.run()
