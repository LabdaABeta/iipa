#!/usr/bin/env python3

# pylint: disable=W0613
# pylint: disable=C0413

import re
import sqlite3

"""
Shmigan Dictionary Data

This module contains classes to work with shmigan data from a database.

Each of the entities in the database derive from the ShmiganObject which
provides methods to access and manipulate them.
"""

DB = sqlite3.connect('shmigan.db')
DB.row_factory = sqlite3.Row

class ShmiganObject:
    """
    This as a direct reference to some object in the database. All specific
    objects inherit from this object.
    """

    _tabname = None
    _idname = 'id'
    _refs = {}

    def __init__(self, idx):
        self._idx = idx
        self._cache = {}

    def __setattr__(self, attr, val):
        object.__setattr__(self, attr, val)

        if attr[0] == '_':
            return

        DB.execute(
            f'UPDATE {self._tabname} SET {attr} = ? WHERE {self._idname} = ?',
            [val if attr not in self._refs else val._idx, self._idx])

        self._cache[attr] = val

    def __getattr__(self, attr):
        if attr in self._cache:
            return self._cache[attr]

        cur = DB.execute(
            f'SELECT * FROM {self._tabname} WHERE {self._idname} = ?',
            [self._idx])
        if not cur:
            raise AttributeError

        row = cur.fetchone()
        if not row:
            raise AttributeError

        for col in row.keys():
            if col in self._refs:
                self._cache[col] = self._refs[col](row[col])
            else:
                self._cache[col] = row[col]

        return self._cache[attr]

    def delete(self):
        DB.execute(
            f'DELETE FROM {self._tabname} WHERE {self._idname} = ?',
            [self._idx])

    @classmethod
    def text_search(cls, fields, text):
        results = []
        search_str = '%' + text + '%'
        for field in fields:
            cur = DB.execute(
                f'SELECT {cls._idname} '
                f'FROM {cls._tabname} '
                f'WHERE {field} LIKE ?',
                [search_str])
            for row in cur:
                results.append(cls(row[0]))
        return results

    @classmethod
    def insert(cls, **kwargs):
        cols = ','.join(kwargs.keys())
        vals = ','.join(['?' for _ in kwargs.keys()])
        args = [x._idx if k in cls._refs else x for k, x in kwargs.items()]
        cur = DB.execute(
            f'INSERT INTO {cls._tabname} ({cols}) VALUES ({vals})', args)
        return cls(cur.lastrowid)

    @classmethod
    def all(cls):
        results = []
        cur = DB.execute(f'SELECT {cls._idname} FROM {cls._tabname}')
        for row in cur:
            results.append(cls(row[0]))
        return results

    @classmethod
    def find(cls, cols, id):
        results = []
        query = ' OR '.join([x + ' = ? ' for x in cols])
        cur = DB.execute(
            f'SELECT {cls._idname} FROM {cls._tabname} WHERE {query}',
            [id for _ in cols])
        for row in cur:
            results.append(cls(row[0]))
        return results

class ShmiganPOS(ShmiganObject):
    """
    This is a direct reference to a shmigan part of speech in a database. Any
    updates to this part of speech are applied directly to the database (though
    not committed).
    """
    _tabname = 'Parts_Of_Speech'

class ShmiganWord(ShmiganObject):
    """
    This is a direct reference to a shmigan word in a database. Any updates to
    this word are applied directly to the database (though not committed).
    """
    _tabname = 'Dictionary'

    _sort_repls = [
        ('xl', 'b'), ('lx', 'b'), ('shl', 'Z'), ('lsh', 'Z'), ('thl', 'Y'),
        ('lth', 'Y'), ('xr', 'X'), ('rx', 'X'), ('shr', 'W'), ('rsh', 'W'),
        ('thr', 'V'), ('rth', 'V'), ('spr', 'U'), ('rps', 'U'), ('skr', 'T'),
        ('rks', 'T'), ('str', 'S'), ('rts', 'S'), ('sn', 'R'), ('ns', 'R'),
        ('sl', 'Q'), ('ls', 'Q'), ('sr', 'P'), ('rs', 'P'), ('sx', 'O'),
        ('xs', 'O'), ('ssh', 'N'), ('shs', 'N'), ('sth', 'M'), ('ths', 'M'),
        ('sp', 'L'), ('ps', 'L'), ('sk', 'K'), ('ks', 'K'), ('st', 'J'),
        ('ts', 'J'), ('n', 'I'), ('l', 'H'), ('r', 'G'), ('x', 'F'),
        ('sh', 'E'), ('th', 'D'), ('p', 'C'), ('k', 'B'), ('t', 'A'),
        ('f', 'c'), ('j', 'd'), ('s', 'f'), ('m', 'g'), ('w', 'j'), ('h', 'k'),
        ('i', 'l'), ('e', 'm'), ('a', 'n'), ('u', 'p'), (' ', 'q')
    ]
    _sort_dict = dict(_sort_repls)
    _sort_regex = re.compile('|'.join([sh for (sh, _) in _sort_repls]))
    _valid = [ # Valid shmigan syllable clusters, in replaceable order
        'xl', 'lx', 'shl', 'lsh', 'thl', 'lth', 'xr', 'rx', 'shr', 'rsh',
        'thr', 'rth', 'spr', 'rps', 'skr', 'rks', 'str', 'rts', 'sn', 'ns',
        'sl', 'ls', 'sr', 'rs', 'sx', 'xs', 'ssh', 'shs', 'sth', 'ths',
        'sp', 'ps', 'sk', 'ks', 'st', 'ts', 'n', 'l', 'r', 'x', 'sh', 'th',
        'p', 'k', 't', 'f', 'j', 's', 'm', 'w', 'h', 'i', 'e', 'a', 'u', 'o'
    ]
    _valid_regex = re.compile('(' + '|'.join(_valid) + '| )*')
    _local = [ # Local shmigan syllable clusters, in replaceable order
        'xl', 'lx', 'shl', 'lsh', 'thl', 'lth', 'xr', 'rx', 'shr', 'rsh',
        'thr', 'rth', 'spr', 'rps', 'skr', 'rks', 'str', 'rts', 'sn', 'ns',
        'sl', 'ls', 'sr', 'rs', 'sx', 'xs', 'ssh', 'shs', 'sth', 'ths',
        'sp', 'ps', 'sk', 'ks', 'st', 'ts', 'n', 'l', 'r', 'x', 'sh', 'th',
        'p', 'k', 't', 'i', 'e', 'a', 'u', 'o'
    ]
    _local_regex = re.compile('(' + '|'.join(_local) + '| )*')

    @staticmethod
    def ordered(spelling):
        """Returns a string representing the spelling in a sortable way."""
        return ShmiganWord._sort_regex.sub(
            lambda match: ShmiganWord._sort_dict[match.group(0)], spelling)

    @staticmethod
    def is_valid(spelling):
        """Returns true if all letters in the spelling are valid."""
        return ShmiganWord._valid_regex.match(spelling)

    @staticmethod
    def is_local(spelling):
        """Returns true if all letters in the spelling are local."""
        return ShmiganWord._local_regex.match(spelling)
ShmiganWord._refs = { 'pos': ShmiganPOS, 'root': ShmiganWord }

class ShmiganDM(ShmiganObject):
    """
    This is a direct reference to a shmigan DM in a database. Any updates to
    this DM are applied directly to the database (though not committed).
    """
    _tabname = 'Derivational_Morphologies'

class ShmiganPenta(ShmiganObject):
    """
    This is a direct reference to a shmigan Pentascale in the database. Any
    updates to this Pentascale are applied directly to the database (though not
    committed).
    """
    _tabname = 'Pentascales'
    _refs = {
        'i': ShmiganWord, 'e': ShmiganWord, 'a': ShmiganWord, 'o': ShmiganWord,
        'u': ShmiganWord }

class ShmiganDMPenta(ShmiganObject):
    """
    This is a direct reference to a shmigan DM pentascale in the database. Any
    updates to this DM pentascale are applied directly to the database (though
    not committed).
    """
    _tabname = 'Derivational_Pentascales'
    _idname = 'rowid'
    _refs = {
        'i': ShmiganDM, 'e': ShmiganDM, 'a': ShmiganDM, 'o': ShmiganDM,
        'u': ShmiganDM }

class ShmiganDoublePenta(ShmiganObject):
    """
    This is a direct reference to a shmigan double pentascale in the database.
    Any updates to this double pentascale are applied directly to the database
    (though not committed).
    """
    _tabname = 'Double_Pentascales'
    _refs = {
        'first_i': ShmiganPenta,
        'first_e': ShmiganPenta,
        'first_a': ShmiganPenta,
        'first_o': ShmiganPenta,
        'first_u': ShmiganPenta,
        'second_i': ShmiganPenta,
        'second_e': ShmiganPenta,
        'second_a': ShmiganPenta,
        'second_o': ShmiganPenta,
        'second_u': ShmiganPenta }

