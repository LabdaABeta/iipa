CREATE TABLE IF NOT EXISTS Parts_Of_Speech (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL);

CREATE TABLE IF NOT EXISTS Dictionary (
    id INTEGER PRIMARY KEY,
    spelling TEXT NOT NULL,
    definition TEXT NOT NULL,
    description TEXT,
    pos INTEGER NOT NULL REFERENCES Parts_Of_Speech (id),
    root INTEGER REFERENCES Dictionary (id));

CREATE TABLE IF NOT EXISTS Fixes (
    id INTEGER PRIMARY KEY,
    name TEXT);
INSERT INTO Fixes VALUES (-1, 'Prefix'), (0, 'Infix'), (1, 'Suffix');

CREATE TABLE IF NOT EXISTS Derivational_Morphologies (
    id INTEGER PRIMARY KEY,
    spelling TEXT NOT NULL,
    meaning TEXT NOT NULL,
    description TEXT,
    fix INTEGER NOT NULL REFERENCES Fixes (id));

CREATE TABLE IF NOT EXISTS Derivations (
    full INTEGER NOT NULL REFERENCES Dictionary (id),
    part INTEGER REFERENCES Derivational_Morphologies (id));

CREATE TABLE IF NOT EXISTS Derivational_Pentascales (
    root TEXT NOT NULL,
    name TEXT,
    i_extreme TEXT,
    u_extreme TEXT,
    i INTEGER REFERENCES Derivational_Morphologies (id),
    e INTEGER REFERENCES Derivational_Morphologies (id),
    a INTEGER REFERENCES Derivational_Morphologies (id),
    o INTEGER REFERENCES Derivational_Morphologies (id),
    u INTEGER REFERENCES Derivational_Morphologies (id));

CREATE TABLE IF NOT EXISTS Pentascales (
    id INTEGER PRIMARY KEY,
    root TEXT NOT NULL,
    name TEXT,
    i_extreme TEXT,
    u_extreme TEXT,
    i INTEGER REFERENCES Dictionary (id),
    e INTEGER REFERENCES Dictionary (id),
    a INTEGER REFERENCES Dictionary (id),
    o INTEGER REFERENCES Dictionary (id),
    u INTEGER REFERENCES Dictionary (id));

CREATE TABLE IF NOT EXISTS Double_Pentascales (
    id INTEGER PRIMARY KEY,
    root TEXT NOT NULL,
    name TEXT,
    first_i_extreme TEXT,
    first_u_extreme TEXT,
    second_i_extreme TEXT,
    second_u_extreme TEXT,
    first_i INTEGER REFERENCES Pentascales (id),
    first_e INTEGER REFERENCES Pentascales (id),
    first_a INTEGER REFERENCES Pentascales (id),
    first_o INTEGER REFERENCES Pentascales (id),
    first_u INTEGER REFERENCES Pentascales (id),
    second_i INTEGER REFERENCES Pentascales (id),
    second_e INTEGER REFERENCES Pentascales (id),
    second_a INTEGER REFERENCES Pentascales (id),
    second_o INTEGER REFERENCES Pentascales (id),
    second_u INTEGER REFERENCES Pentascales (id));

CREATE TABLE IF NOT EXISTS Triple_Pentascales (
    id INTEGER PRIMARY KEY,
    root TEXT NOT NULL,
    name TEXT,
    first_i_extreme TEXT,
    first_u_extreme TEXT,
    second_i_extreme TEXT,
    second_u_extreme TEXT,
    third_i_extreme TEXT,
    third_u_extreme TEXT,
    first_i INTEGER REFERENCES Double_Pentascales (id),
    first_e INTEGER REFERENCES Double_Pentascales (id),
    first_a INTEGER REFERENCES Double_Pentascales (id),
    first_o INTEGER REFERENCES Double_Pentascales (id),
    first_u INTEGER REFERENCES Double_Pentascales (id),
    second_i INTEGER REFERENCES Double_Pentascales (id),
    second_e INTEGER REFERENCES Double_Pentascales (id),
    second_a INTEGER REFERENCES Double_Pentascales (id),
    second_o INTEGER REFERENCES Double_Pentascales (id),
    second_u INTEGER REFERENCES Double_Pentascales (id),
    third_i INTEGER REFERENCES Double_Pentascales (id),
    third_e INTEGER REFERENCES Double_Pentascales (id),
    third_a INTEGER REFERENCES Double_Pentascales (id),
    third_o INTEGER REFERENCES Double_Pentascales (id),
    third_u INTEGER REFERENCES Double_Pentascales (id));
