# This is sagemath code
# Income curve setup so that at first any growth is very beneficial (exponential start [dx = adx])
# Then growth becomes tied to value (quadratic middle class [dx = ax])
# Finally growth decays as value becomes 'absurd' (logarithmic higher class [dx = a/x])
# Maintain continuous derivative and function to ensure smoothness.

# Line is income, value is effective outcome
# i.e. when you make x_line income you are worth x_value and switch to next bracket after x
def income_curve(ubi, poverty_line, poverty_value, upper_line, upper_value, high_rate):
    x = var('x')
    
    poverty_rate = ln(poverty_value/ubi)/poverty_line
    poverty = ubi * e**(poverty_rate * x)
    print('poverty:', poverty)
    print('dpoverty:', poverty.diff(x))
    
    initial_mid_rate = ubi * poverty_rate * e**(poverty_rate * poverty_line)
    print('i_m_r:', initial_mid_rate)
    mid_rate = ((upper_value - poverty_value) - initial_mid_rate * (upper_line - poverty_line)) / ((upper_line - poverty_line) ** 2)
    print('m_r:', mid_rate)
    middle = mid_rate * (x - poverty_line) ** 2 + initial_mid_rate * (x - poverty_line) + poverty_value
    print('middle:', middle)
    print('dmiddle:', middle.diff(x))
    
    upper_rate = middle.diff(x).substitute(x == upper_line)
    print('upper_rate:', upper_rate)
    upper = upper_rate * upper_line * log(high_rate * x) + upper_value - upper_rate * upper_line * log(high_rate * upper_line)
    print('upper:', upper)
    print('dupper:', upper.diff(x))
    
    return piecewise([[(-oo,poverty_line),poverty],[[poverty_line,upper_line],middle],[(upper_line,oo),upper]])

curve = income_curve(10000,20000,25000,50000,80000,5000)
pieces = [[support, function.diff(x)] for support, function in curve.items()]
dcurve = piecewise(pieces)
plot(curve,xmin=0,xmax=100000)